/** @type {import('tailwindcss').Config} */
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: ['./src/**/*.{html,js}'],
  theme: {
    screens: {
      'xs': '320px',
      ...defaultTheme.screens
    },
    extend: {
      fontFamily: {
        'kumbh-sans': ['Kumbh Sans', 'sans-serif']
      },
      fontSize: {
        'custom': '0.8125rem'
      },
      backgroundPosition: {
        'custom': '-603px -350px'
      },
      backgroundImage: {
        'mobile-pattern': 'url("./images/bg-pattern-mobile.svg")',
        'desktop-pattern': 'url("./images/bg-pattern-desktop.svg")'
      },
      colors: {
        'soft-violet': 'hsl(273, 75%, 66%)',
        'soft-red': 'hsl(14, 88%, 65%)',
        'soft-blue': 'hsl(240, 73%, 65%)',
        'grayish-blue': 'hsl(240, 5%, 91%)'
      },
      spacing: {
        '116': '36.5rem',
        '98': '22.4375rem',
      }
    },
  },
  plugins: [],
}
